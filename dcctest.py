#!/usr/bin/env python

version = '0.1'
name = 'dcctest'

import weechat

weechat.register(name, version, '', 'print dcc events')

weechat.add_message_handler('weechat_dcc', 'dcchandler')
weechat.add_message_handler('DCC', 'dcchandler')
weechat.add_message_handler('weechat_pv', 'privhandler')

def print_info(string):
    weechat.prnt("\00303" + str(string) + "\003")


def dcchandler(server, args):
    print_info("dcc msg received:")
    print_info("server: <%s>" % str(server))
    print_info("args: <%s>" % str(args))
    return weechat.PLUGIN_RC_OK

def privhandler(*args):
    print_info("message received: <%s>" % str(args))
    return weechat.PLUGIN_RC_OK

