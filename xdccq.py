#!/usr/bin/env python

# written by jmoiron, jmoiron.net
# licensed under the GNU GPL v2
# a copy of the license can be found:
#   http://www.gnu.org/licenses/gpl.txt

email_address = 'am1vaXJvbkBqbW9pcm9uLm5ldA==\n'.decode('base64')

class Weechat(object):
    """Fake xchat object.  For testing via command line."""
    def __nonzero__(self):
        """Make this boolean False:
        >>> bool(Weechat())
        False
        """
        return False
    def prnt(self, s):
        s = s.replace('\00302', '')
        s = s.replace('\00303', '')
        s = s.replace('\002', '')
        s = s.replace('\003', '')
        print s
    command=prnt
    def __getattr__(self, name):
        def lambda_(*x, **y):
            print '%s: %s, %s' % (name, x, y)
        return lambda_

try: import weechat
except ImportError: weechat = Weechat()
import sys
import re
import threading
import time
import wsgiref, wsgiref.simple_server, wsgiref.util
from exceptions import ValueError

__module_name__ = "xdccq"
__module_version__ = "0.6"
__module_description__ = "Xdcc Queue"

# to enable some debugging output, set to True
__debugging__ = True

colors = {
    'black'   : '01', 'dkblue' : '02',  'dkgreen' : '03', 'red'     : '04',
    'brown'   : '05', 'purple' : '06',  'orange'  : '07', 'yellow'  : '08',
    'ltgreen' : '09', 'aqua'   : '10',  'ltblue'  : '11', 'blue'    : '10',
    'violet'  : '13', 'grey'   : '14',  'ltgrey'  : '15', 'white'   : '16'
}

# for americans
colors['gray'], colors['ltgray'] = colors['grey'], colors['ltgrey']

if __debugging__:
    import traceback

def pcolor(string, color):
    colorstr = '\003%s' % colors.get(color, 'black')
    print colorstr + str(string) + '\003'

def print_debug(string):
    if __debugging__:
        pcolor(string, 'orange')

def print_error(string): pcolor(string, 'red')
def print_help(string): pcolor(string, 'blue')
def print_success(string): pcolor(string, 'dkgreen')
def echo(string): pcolor(string, 'ltblue')

print_info = print_success

cmd_help = {}
gen_help = [
    "\037[help, ?]\037 <cmd>    - prints this help or detailed help for 'cmd'\n",
    "\037[ls, list]\037         - lists files in queue\n",
    "\037get\037 [bot] [#, #-#] - adds 'send' cmds to queue\n",
    "\037rm\037 [bot] <#, #-#>  - removes 'send' cmds from queue\n",
    "\037[cancel,stop]\037      - cancles current transfer",
]
cmd_help['msg'] = "   " + "   ".join(gen_help)
cmd_help['?'] = """\n\002/xdccq\002 \037[help, ?]\037 <cmd>\n   \00303By itself, help/? prints out the available commands along withshort descriptions of each.  If you supply a command \002cmd\002, a longer help description of that command and its usage is given.\003"""
cmd_help['ls'] = """\n\002/xdccq\002 \037[ls, list]\037\n   \00303Lists the file gets currently in the queue.\00303"""
cmd_help['get'] = """\n\002/xdccq\002 \037get\037 [bot] [#, #-#]\n   \00303Adds commands to the local queue "/ctcp \037bot\037 xdcc send \037#\037".  A number ["7"], a number range ["1-5"], or a comma separated list ["7, 8-11, 15"] may be given.\003"""
cmd_help['rm'] = """\n\003/xdccq\002 \037rm\037 [bot] <#, #-#>\n   \00303Removes commands from local queue.  If only \037bot\037 is supplied, it removes all commands dealing with \037bot\037.  If it is supplied with a number or number range, any commands in that range are removed.   If you had numbers "8, 9, 11, 12, 14" queued, and removed "8-14", it would remove all of those package numbers for that bot without resulting in an error for the missing "10" and "13".\003\n   \00303New in 0.3: rm will now also call 'cancel' if the active transfer is from [bot] and the optional range argument was not given.\003"""
cmd_help['cancel'] = """\n\003/xdccq\002 \037cancel\037\n   \00303If a DCC handled by xdccq is currently active, it cancels that transfer and starts the next one on the queue.  If the 'active' transfer is remotely queued, xdccq attempts to unqueue it by issuing a '/msg <bot> xdcc remove' command.  If this command fails, this transfer will continue to remotely pend; take notice!\003"""

# converts a string like "3,5,7-9,14" into a list
# raises InvalidRange
# raises InvalidInteger
def numToList(string):
    """Converts a string like '3,5,7-9,14' into a list."""
    ret = []
    numsplit = string.split(",")
    # the following code makes nums into a list of all integers
    for n in numsplit:
        nr = n.split('-')
        # handle the case of a single number
        if len(nr) == 1:
            try: ret.append(int(n))
            except: raise ValueError("number")
        # handle the case of a range
        elif len(nr) == 2:
            try:
                low = int(nr[0])
                high = int(nr[1]) + 1
                if low > high: raise ValueError("number")
                ret += range(low, high)
            except ValueError: raise ValueError("number")
        else: raise ValueError("range")
    return ret

class Context(object):
    def __init__(self, server, channel):
        self.server = server
        self.channel = channel

    def command(self, s):
        return weechat.command(str(s), self.channel, self.server)

class Command:
    def __init__(self, bot, num):
        self.bot = str(bot)
        self.num = int(num)
        self.channel = weechat.get_info("channel")
        self.network = weechat.get_info("server")
        self.server = self.network
        self.filename = ""
        self.retries = 0
        self.queue_position = 0
        self.queued = False
        self.transfering = False
        self.dead = False
        self.s = "/ctcp %s xdcc send #%d" % (str(bot), int(num))
        self.context = Context(self.server, self.channel)

    def command(self, cmd):
        #print_debug("weechat.command('%s', '', '%s')" % (str(cmd), self.network))
        weechat.command(str(cmd), "", self.network)

    def __str__(self):
        return "#%d on %s (%s)" % (self.num, self.bot, self.channel)

    def execute(self, retry=False):
        if retry: self.retries += 1
        self.context.command(self.s)

    def retry(self):
        if self.retries < 3:
            self.execute(True)
            return True
        return False

    def dccclose(self):
        if self.transfering:
            s = "/dcc close get %s %s" % (self.bot, self.file)
            print_debug("/%s" % (s))
            self.context.command(s)

    def dequeue(self):
        if self.queued:
            s = "/msg %s xdcc remove" % (self.bot)
            print_debug("/%s" % (s))
            self.context.command(s)

class Queue:
    def __init__(self):
        self.data = []

    def put(self, item):
        self.data.append(item)

    def get(self):
        tmp = self.data[0]
        del self.data[0]
        return tmp

    def __getitem__(self, key):
        return self.data[key]

    def __delitem__(self, key):
        del self.data[key]

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        return self.data.__iter__()

    def remove(self, item):
        self.data.remove(item)

    def append(self, item):
        self.data.append(item)

class CmdQueue(Queue):
    """Utility functions for dealing with a queue of Commands"""

    def get_pack_dict(self):
        r = {}
        for cmd in self.data:
            r.setdefault(cmd.bot, []).append(cmd)
        return r

    def getBotSet(self):
        """Returns a list of bots with packages in the queue."""
        return list(set([cmd.bot.lower() for cmd in self.data]))

    def getBotPackSet(self, botname, r=[]):
        """Gets the pack set in the Queue for a bot."""
        packs = [cmd for cmd in self.data if cmd.bot.lower() == botname.lower()]
        # if supplied a range, cut list to those in the range
        if r: packs = [cmd for cmd in packs if cmd.num in r]
        return packs

    def removeBot(self, botname, r=[]):
        """Remove the packs from bot `botname` with numbers in `r`."""
        packs = self.getBotPackSet(botname, r)
        for cmd in packs:
            self.data.remove(cmd)
        # this might be better as the actual list someday?
        return len(packs)

    def transfering(self):
        """Return the actively transfering commands."""
        return [cmd for cmd in self.data if cmd.transfering]

    def queued(self):
        """Return the queued commands."""
        return [cmd for cmd in self.data if cmd.queued]

    def cancel(self, bot, r=[]):
        items = self.getBotPackSet(bot, r)
        for item in items:
            item.dccclose()
            item.dequeue()
            self.remove(item)
        return items

class Irc(object):
    @staticmethod
    def humanize(cps):
        """Humanize a string of cps."""
        units = ['Bps', 'KBps', 'MBps']
        # order of magnitude
        reduce_factor = 1024.0
        oom = 0
        while cps /(reduce_factor**(oom+1)) >= 1:
            oom += 1
        return '%0.1f %s' % (cps/reduce_factor**oom, units[oom])

    @staticmethod
    def getCurrentBotCPS(bot):
        status = {0: 'waiting', 1: 'connecting', 2: 'active', 3: 'finished',
                4: 'failed', 5: 'interrupted by user'}
        dcclist = weechat.get_dcc_info()
        dcclist = [i for i in dcclist if i['nick'].lower() == bot.lower()]
        for item in dcclist:
            # waiting, connecting
            if item['status'] in (0, 1):
                return '0 CPS (%s)' % (status[item['status']])
            elif item['status'] == 2:
                return '%s CPS' % (str(item['cps']))
        return 'Unknown CPS'

    @staticmethod
    def get_bot_xfer_string(bot):
        status = {0: 'waiting', 1: 'connecting', 2: 'active', 3: 'finished',
                4: 'failed', 5: 'interrupted by user'}
        dcclist = weechat.get_dcc_info()
        dcclist = [i for i in dcclist if i['nick'].lower() == bot.lower()]
        for item in dcclist:
            if item['status'] in (0, 1):
                return '(%s)' % (status[item['status']])
            elif item['status'] == 2:
                return Irc.make_xfer_line(item['cps'], item['pos'], item['size'])
        return 'Unknown CPS'

    @staticmethod
    def make_xfer_line(cps, pos, size):
        cps, pos, size = int(cps), int(pos), int(size)
        remainder = size - pos
        eta = remainder / float(cps)
        pct_done = int(100 * pos/float(size))
        whole = pct_done / 10
        fraction = '.' if (pct_done % 10) > 4 else ' '
        return '%s [%s%s%s] (eta: %0.2fs)' %\
            (Irc.humanize(cps), ':'*whole, fraction, ' '* (10 - (whole+1)), eta)


# New feature from xchat version, multiqueue:  'Active' transfers are now a
# queue with one possible transfer per bot.  set MULTIQUEUE = False to disable
MULTIQUEUE = True

CommandQueue = CmdQueue()
Active = CmdQueue()
Watchdog = False

# help, ?  -- print out explanations of commands
def help(a):
    global cmd_help
    if len(a) == 3:
        if cmd_help.has_key(a[2]):
            print_help(cmd_help[a[2]])
            return
    usage()
    weechat.prnt(cmd_help['msg'])

def debug(a):
    """Test out code here."""
    info = weechat.get_dcc_info()
    print_debug(info)
    for item in info:
        print_debug(item)

def ls(a):
    """ ls, list -- print out a list of files in the queue"""
    global CommandQueue, Active
    s = []
    for cmd in Active.transfering():
        cps = Irc.get_bot_xfer_string(cmd.bot)
        s.append("Pack #%d from %s on %s@%s transfered at %s." % (cmd.num, cmd.bot, cmd.channel, cmd.network, cps))
    for cmd in Active.queued():
        s.append("Pack #%d from %s on %s@%s queued remotely [position %d]." % (cmd.num, cmd.bot, cmd.channel, cmd.network, cmd.queue_position))
    if not s:
        print_info("No files being transfered.")
    else:
        for line in s: print_info(line)
    if len(CommandQueue) == 0:
        print_info("No files in the queue.")
    else:
        print_info("Queued packs:")
        for bot, cmds in CommandQueue.get_pack_dict().items():
            print_info("  %s: %s" % (bot, [c.num for c in cmds]))

def get(a):
    """get -- add commands to the queue"""
    if len(a) != 4:
        print_error("Error: invalid arguments for get.  /xdccq get [bot] [#, #-#]")
        return
    bot = str(a[2])
    try: nums = numToList(a[3])
    except ValueError, exc:
        # exc.args[0] is in the set ['number', 'range']
        print_error("Error: %s contains invalid %s." % (a[3], exc.args[0]))
        return
    print_info("adding packs for bot [%s]: %s" % (bot, str(nums)))
    for num in nums:
        CommandQueue.put(Command(bot, num))
    # if we actually added something, try to start this party
    if len(nums):
        run()

# rm -- remove commands from the queue  
def rm(a):
    global CommandQueue, Active
    if len(a) not in (3, 4):
        print_error("Error: invalid arguments for 'rm'.  /xdccq rm [bot] <#, #-#>")
        return
    items_to_delete = []
    if len(a) == 4:
        try: items_to_delete = numToList(a[3])
        except ValueError, exc:
            # exc.args[0] is in the set ['number', 'range']
            print_error("Error: %s contains invalid %s." % (a[3], exc.args[0]))
            return
    # if we removed all from bot, and the bot is currently transfering...
    removed = CommandQueue.removeBot(a[2], items_to_delete)
    active_removed = Active.cancel(a[2], items_to_delete)
    info = 'deleted %d commands from the wait queue.' % removed
    if active_removed:
        info = info[:-1] + ', and %d from the active.' % len(active_removed)
    print_info(info)

# we won't be needing a, and it'l clean the call elsewhere
def cancel(a=[]):
    global Active
    if not Active:
        print_error("No currently transfering packages.")
        return
    for item in Active.transfering():
        item.dccclose()
        Active.remove(item)
    for item in Active.queued():
        item.dequeue()
        Active.remove(item)
    run()

def multiq(a=[]):
    global MULTIQUEUE
    MULTIQUEUE = not MULTIQUEUE
    if MULTIQUEUE: s = 'on'
    else: s = 'off'
    print_info("Multiq mode %s" % s)

USAGE_STR = "Usage: \002/xdccq\002 [cmd] [args], \002/xdccq\002 \037help\037 for commands."

def usage():
    weechat.prnt(USAGE_STR)

def exe(args):
    exstr = ' '.join(args[2:])
    print_debug(">>> %s" % exstr)
    res = eval(exstr)
    print_debug("<<< %s" % str(res))

def dispatch(server, args):
    args = "/xdccq " + args
    split = args.split(' ')
    try:
        {
        "debug"   : debug,
        "help"    : help,
        "?"       : help,
        "ls"      : ls,
        "list"    : ls,
        "get"     : get,
        "rm"      : rm,
        "cancel"  : cancel,
        "stop"    : cancel,
        "multiq"  : multiq,
        "exec"    : exe,
    }[split[1]](split)
    except:
        if __debugging__: print_debug(traceback.format_exc())
        else: usage()
    return 0

# watchdog callback
def transferCheck(data=None):
    """This watchdog function runs as long as we are transferring something.  It
    goes through the active queue looking for packs that are neither transferring
    nor queued.  If it finds one as such, it marks it as 'dead'.  If it stays 'dead'
    until the next run, it will retry the request.  If the retry threshold (3) is
    met, the command will be removed."""
    true, false = weechat.PLUGIN_RC_OK, weechat.PLUGIN_RC_KO
    global Active
    # if active has already finished, stop the timer
    if not Active:
        weechat.remove_timer_handler("transferCheck")
        return false
    for cmd in Active:
        if cmd.transfering or cmd.queued:
            continue
        if not cmd.dead:
            cmd.dead = True
        elif cmd.retry():
            print_error("Previous attempt to get file (#%d from %s) failed.  Repeating (%d of 3 retries)" % (cmd.num, cmd.bot, cmd.retries))
        else:
            print_info("At this point we'd want to remove (#%d from %s, %d)." % (cmd.num, cmd.bot, cmd.retries))
            # run()
    return true

def run():
    """This is the logic on what packs actually get added to the queue.  It's
    run just about any time there is an interaction with the queue (get, delete,
    dcc events, etc)."""
    global CommandQueue, Active, Watchdog
    if not MULTIQUEUE:
        # If there's an active transfer, we return
        if Active: return
        if not CommandQueue: return
        # If not, we start one and start a watchdog timer
        cmd = CommandQueue.get()
        Active.append(cmd)
        cmd.execute()
        if not Watchdog:
            Watchdog = weechat.add_timer_handler(45, "transferCheck")
        return
    # We are in MULTIQUEUE mode ...
    aps = sorted(Active.getBotSet())
    cps = sorted(CommandQueue.getBotSet())
    missing = [bot for bot in cps if bot not in aps]
    print_debug('multiq: active: %s, queued: %s, missing: %s' % (aps, cps, missing))
    # if we have the same bots in each, we are already transfering at full..
    if not missing:
        return
    for bot in missing:
        cmd = CommandQueue.getBotPackSet(bot)
        if not cmd: return
        cmd = cmd[0]
        Active.append(cmd)
        CommandQueue.remove(cmd)
        print_debug("%s on %s@%s" % (cmd.s, cmd.channel, cmd.network))
        cmd.execute()
    # set up a watchdog every 45 seconds
    if Active and not Watchdog:
        Watchdog = weechat.add_timer_handler(45, "transferCheck")


""" some other messages are possible:
** Closing Connection: Unable to transfer data (Broken pipe)
** Closing Connection: DCC Timeout (180 Sec Timeout)
"""
def notice(split, full, data):
    global CommandQueue, Active
    # ['jonas|srvr', 'Total Offered: 0.3 MB  Total Transferred: 0.30 MB']
    print_debug("notice: %s, %s, %s" % (split, full, data))
    botname = split[0]
    message = split[1]
    if not Active: return
    if Active.queued: return
    if Active.bot == botname:
        re_queued = re.compile(r"queue")
        re_pos = re.compile(r"position [0-9]+")
        if re_queued.search(message.lower()):
            Active.queued = True
            print_info("xdccq detected the active file being placed on a remote queue")
        if re_pos.search(message.lower()):
            try:
                res = re_pos.search(message.lower())
                postr = message[res.start() : res.end()]
                pos = int(postr.split()[1])
                Active.queue_position = pos
            except:
                Active.queue_position = 0
                print_error("an error occured parsing the remote queue position")


# hack;  it binds this at a weird time so 'Active' local is not bound
def getActive():
    return Active

def dcc_check():
    """There's no received event, so we periodically check the full list of
    dcc items for finished files.  If one of the finished files matches a
    file in our Active queue, we remove it and 'run' to fill up the queue
    again."""
    Active = getActive()
    matched = False
    if not Active: return 0
    dcclist = weechat.get_dcc_info()
    done = [d for d in dcclist if d['status'] > 2]
    for cmd in Active:
        packs_for_bot = [d for d in done if d['nick'].lower() == cmd.bot.lower()]
        for pack in packs_for_bot:
            if pack['remote_file'] == cmd.filename:
                print_info("Received file \"%s\" from %s on %s@%s." %\
                        (cmd.filename, cmd.bot, cmd.channel, cmd.network))
                Active.remove(cmd)
                matched = True
    if matched: run()
    return 0

def dcc_handle(server, args):
    """About the only events we get from weechat is a dcc send event, from
    which we can parse the bot and filename.  We use this to set the info
    on any matching bot in the Active list (even if this isn't the
    pack corresponding to that pack-num;  there's just no way for xdccq to
    know if you have started other transfers manually with the bot)."""
    # 0 = '', 1 = bot PRIVMSG nick, 2 = DCC SEND filename
    # if we do not have a transfer going, it's not something we initiated
    if not Active: return 0
    spl = args.split(':')
    dccmsg = spl[2].strip('\x01').split()
    print_debug("dcc_handle: %s, %s" % (server, dccmsg))
    found = False
    for cmd in Active:
        if spl[1].lower().startswith(cmd.bot.lower()):
            found = True
            print_info('Pack #%s <%s> started from %s' % (cmd.num, dccmsg[2], cmd.bot))
            cmd.filename = dccmsg[2]
            cmd.transfering = True
            cmd.queued = False
    if not found:
        print_debug('%s received but did not match active bots (%s)' % (dccmsg, Active.getBotSet()))
    return 0

weechat.register(__module_name__, __module_version__, '', __module_description__)
weechat.add_command_handler('xdccq', 'dispatch')
weechat.add_message_handler('weechat_dcc', 'dcc_handle')
weechat.add_timer_handler(3, "dcc_check")

print_info("XdccQ-TNG loaded successfully")
usage()

