#!/usr/bin/env python

import urllib, urllib2, BeautifulSoup

version = '0.1'
name = 'tinyurl'

try:
    import weechat
    weechat.register(name, version, '', 'turn urls into tiny urls')
    weechat.add_message_handler('privmsg', 'msg_handler')
except ImportError:
    print "error importing weechat"
    class weechat:
        PLUGIN_RC_OK = 1
        @staticmethod
        def prnt(s): print s

url = 'http://tinyurl.com/create.php'

def print_info(string):
    weechat.prnt("\00303" + str(string) + "\003")

def msg_handler(server, args):
    #  [('highway', ':purple_lizard!~plizard@25ea667f.296d6466.dc.cox.net PRIVMSG #lurk:!list')]
    nick = args.split(':')[1].split('!')[0]
    chan = args.split()[2].split(':')[0]
    msg  = ':'.join(args.split(':')[2:])
    if msg.lower().startswith('!tinyurl'):
        bigurl = msg.split()[1]
        print_info("getting tiny url for < %s >" % bigurl)
        tiny = get_tinyurl(bigurl)
        weechat.command('/say %s' % tiny, chan, server)
    return weechat.PLUGIN_RC_OK

def get_tinyurl(bigurl):
    query = urllib.urlencode({'url':bigurl})
    req = urllib2.Request(url, data=query)
    ret = urllib2.urlopen(req)
    soup = BeautifulSoup.BeautifulSoup(ret.read())
    tiny = soup.body.findChildren('blockquote')[1].findChildren('a')[0]['href']
    return tiny

